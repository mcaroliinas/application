﻿using System.Web.Mvc;

namespace Exercise_Bliss.Controllers
{
    public class ConnectivityController : Controller
    {
        // GET: Connectivity
        public ActionResult ConnectivityView()
        {
            bool networkUp = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();

            string url = HttpContext.Request.UrlReferrer.AbsoluteUri;
            string oldUrl = string.Empty;
            if (!url.Contains("Connectivity"))
                oldUrl = HttpContext.Request.UrlReferrer.AbsoluteUri;

            if (!networkUp)
                return View();
            else
                return Redirect(oldUrl);
        }
    }
}