﻿using Exercise_Bliss.Models;
using MySql.Data.MySqlClient;
using System;
using System.Web.Mvc;

namespace Exercise_Bliss.Controllers
{
    public class LoadingController : Controller
    {
        // GET: Loading
        public ActionResult LoadingView()
        {
            LoadingModel loadingModel = new LoadingModel();
            loadingModel.Connected = CheckConnection();

            if (loadingModel.Connected)
                return RedirectToAction("ListTableView", "List");

            return View(loadingModel);
        }

        public static bool CheckConnection()
        {
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings["toConnect"].ConnectionString;
            bool isConn = false;
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(connection);
                conn.Open();
                isConn = true;
            }
            catch (ArgumentException ex)
            {
                isConn = false;
            }
            catch (MySqlException ex)
            {
                isConn = false;
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return isConn;
        }

        public ActionResult GoTo()
        {
            LoadingModel loadingModel = new LoadingModel();
            loadingModel.Connected = CheckConnection();

            if (loadingModel.Connected)
                return RedirectToAction("ListTableView", "List");

            return View();
        }

        public ActionResult TryAgain()
        {
            return RedirectToAction("LoadingView", "Loading");
        }
    }
}