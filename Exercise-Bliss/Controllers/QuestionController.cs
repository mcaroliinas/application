﻿using Exercise_Bliss.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Data.SqlClient;

namespace Exercise_Bliss.Controllers
{
    public class QuestionController : Controller
    {
        int pageSize = 10;

        // GET: Question
        public ActionResult QuestionView(int? page, string question_filter)
        {
            List<question> listToReturn = new List<question>();
            using (DBModelsApplication dbModel = new DBModelsApplication())
            {
                listToReturn = dbModel.question.Where(x => x.Question1.Contains(question_filter) || string.IsNullOrEmpty(question_filter)).ToList<question>();
            }

            int pageNumber = (page ?? 1);

            return View(listToReturn.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int id)
        {
            question q = new question();
            using (DBModelsApplication dbModel = new DBModelsApplication())
            {
                q = dbModel.question.Where(x => x.idQuestion == id).FirstOrDefault();
            }
            return View();
        }
    }
}