﻿using Exercise_Bliss.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using System.Net.Mail;
using System.Net;

namespace Exercise_Bliss.Controllers
{
    public class ListController : Controller
    {
        int pageSize = 10;

        // GET: List
        public ActionResult ListTableView(int? page, string searching)
        {
            if (!LoadingController.CheckConnection())
                return RedirectToAction("LoadingView", "Loading");

            List<question> listToReturn = new List<question>();
            using (DBApplicationModels dbModel = new DBApplicationModels())
            {
                listToReturn = dbModel.question.Where(x => x.Question1.Contains(searching) || x.Answer.Contains(searching) || string.IsNullOrEmpty(searching)).ToList<question>();
            }

            int pageNumber = (page ?? 1);

            return View(listToReturn.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int id)
        {
            if (!LoadingController.CheckConnection())
                return RedirectToAction("LoadingView", "Loading");

            question q = new question();
            using (DBApplicationModels dbModel = new DBApplicationModels())
            {
                q = dbModel.question.Where(x => x.idQuestion == id).FirstOrDefault();
            }

            return View(q);
        }

        public ActionResult SaveDetail(question q)
        {
            if (!LoadingController.CheckConnection())
                return RedirectToAction("LoadingView", "Loading");

            using (DBApplicationModels dbModel = new DBApplicationModels())
            {
                dbModel.Entry(q).State = System.Data.Entity.EntityState.Modified;
                dbModel.SaveChanges();
            }

            return RedirectToAction("ListTableView", "List");
        }

        public ActionResult SendEmail()
        {
            if (!LoadingController.CheckConnection())
                return RedirectToAction("LoadingView", "Loading");

            var fromMail = System.Configuration.ConfigurationManager.AppSettings["FromMail"];
            var password = System.Configuration.ConfigurationManager.AppSettings["FromPassword"];
            var toMail = System.Configuration.ConfigurationManager.AppSettings["ToMail"];

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(fromMail);
            mail.To.Add(new MailAddress(toMail));
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(fromMail, password);
            client.EnableSsl = true;
            mail.Subject = "Share URL";
            mail.Body = HttpContext.Request.UrlReferrer.AbsoluteUri;
            client.Send(mail);

            if (HttpContext.Request.UrlReferrer.LocalPath.Contains("Details"))
                return RedirectToAction("Details", "List");
            else
                return RedirectToAction("ListTableView", "List");
        }
    }
}