#DATABASE
My application use MYSQL database.

#Script
CREATE DATABASE 'application';
 
CREATE TABLE 'application'.'question' (
  'idQuestion' int(11) NOT NULL AUTO_INCREMENT,
  'Question' varchar(200) NOT NULL,
  'Number' int(11) NOT NULL,
  'Answer' varchar(5) NOT NULL,
  PRIMARY KEY ('idQuestion')
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Today is a good day?', '1', 'Yes');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Are you fine?', '2', 'No');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Do you cook lasagna?', '3', 'Yes');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Do you like this city?', '4', 'Yes');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Do you like this country?', '5', 'No');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Are you happy?', '6', 'Yes');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Are you hungry?', '7', 'Yes');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Can I learn English quickly?', '8', 'Yes');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Do you know him?', '9', 'Yes');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Did you order the pizza?', '10', 'No');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('This color is her favourite colour?', '11', 'No');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Do we need a nanny?', '12', 'No');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Are they always late?);
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Does he complain all the time?', '14', 'No');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Is his birthday?', '15', 'No');
INSERT INTO 'application'.'question' ('Question', 'Number', 'Answer') VALUES ('Are we going to finish?', '16', 'No');

#WebConfig
It's necessary change in webconfig the properties: FromMail, ToMail, FromPassword on appsettings and toConnect, DBApplicationModels on connectionStrings.

#Email
In the from email, it's necessary Allow less secure app to YES